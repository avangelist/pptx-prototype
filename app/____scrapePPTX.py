""" creating an app to open a presentation """
# importing Presentation gives you an object that is the route
from pptx import Presentation

# Load a Presentation
prs = Presentation("tmp/Coaching101.pptx")

# put the slides into a variable to make it easier to write
slidesArray = prs.slides

text_runs = []

for slide in prs.slides:
    for shape in slide.shapes:
        if not shape.has_text_frame:
            continue
        for paragraph in shape.text_frame.paragraphs:
            for run in paragraph.runs:
                text_runs.append(run.text)

        print(text_runs)
# get number of slides print(len(slidesArray))
