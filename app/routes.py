import os
import urllib.request
from flask import render_template, flash, request, redirect, url_for
from app import app
from app.forms import UploadForm
from zipfile import ZipFile
from lxml import etree
from werkzeug.utils import secure_filename
from http import cookies

# Variables set for the upload file function
UPLOAD_FOLDER = "tmp/"
ALLOWED_EXTENSIONS = "pptx"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def make_transform(name, parser):
    # https://lxml.de/xpathxslt.html#xslt
    with open(name) as f:
        # For base_url: https://lxml.de/parsing.html#parsers
        xslt_root = etree.parse(f, parser, base_url='')

        transform = etree.XSLT(xslt_root)
        return transform

# index for site function
@app.route('/')
@app.route('/index')
def index():
    return render_template("index.html")

# upload form function
def allowed_file(filename):
    return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# open the uploader page
@app.route('/upload')
def upload_form():
    return render_template('upload.html')

# when the page has been submitted
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # Check if the post request has a file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if there's no file selected, or submitted with an empty part without a filename
        if file.filename == '':
            flash('No file selected')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            flash('File uploaded')

            return redirect(url_for('results', filename=filename))

# results page showing the output from the uploaded file
@app.route('/results/<filename>', methods=['GET', 'POST'])
def results(filename):
    filename = filename
    fileSubmitted = UPLOAD_FOLDER + filename
    prs = ZipFile(fileSubmitted, 'r')
    slide_contents = []

    with prs as pptx:
        parser = etree.XMLParser()

        # Have the transform use the custom resolver.
        xsl_name = os.path.join("app/static/getalt.xsl")
        transform = make_transform(xsl_name, parser)

        # Now process all the slides.
        for name in pptx.namelist():
            if name.startswith('ppt/slides/slide'):

                slidefile = name
                with pptx.open(name) as f:
                    slide = etree.parse(f, base_url='')
                    # This is a silent gotcha in the style sheet.
                    # For strparam see: https://lxml.de/xpathxslt.html#xpath
                    result = transform(
                        slide,
                        slidefile=etree.XSLT.strparam(slidefile),
                        pptfile=etree.XSLT.strparam(os.path.basename(fileSubmitted))
                    )

                    # print(str(result))
                    slide_contents.append(str(result))

    #remove the uploaded file from the tmp dir
    os.remove(fileSubmitted)
    return render_template("results.html", slide_contents = slide_contents, fileSubmitted = fileSubmitted)
